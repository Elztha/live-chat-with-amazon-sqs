# Live chat with Amazon SQS


__Amazon Simple Queue Service (SQS)__ is a fully managed message queuing service that enables you to decouple and scale microservices, distributed systems, and serverless applications. SQS eliminates the complexity and overhead associated with managing and operating message oriented middleware, and empowers developers to focus on differentiating work. Using SQS, you can send, store, and receive messages between software components at any volume, without losing messages or requiring other services to be available. 


## Scenario & Architect
In this tutorial, we will try to make a simple live chat between 2 users in 2 differents EC2 in same region. By through to SQS, we will send message. 

<p align="center">
    <img src="./Images/0.jpg" alt="1.jpg" width="40%" height="40%">
</p>

## Prerequisites

> Prepare an AWS account. </br>

> Make sure your are in US East (N. Virginia), which short name is us-east-1.

> Download file [website.zip](./Materials/website.zip) and please to extract it

> Create VPC that has 2 subnets __PUBLIC__ connect internet gateway and security group *all traffic* inbound


## Lab tutorial

### Prepare the SQS 

1. Sign in to the AWS Management Console, and then open [AWS Cloud9 console](https://console.aws.amazon.com/cloud9/).

2. Click __Get Started Now__

<p align="center">
    <img src="./Images/1.jpg" alt="1.jpg" width="40%" height="40%">
</p>

3. Type Queue Name with __Yourname-SQS__.
4. Select __Standard Queue__ then click __Quick Create-Queue__.

<p align="center">
    <img src="./Images/2.jpg" alt="1.jpg" width="40%" height="40%">
</p>

5. Take a note the __Queue URL__ at below Tab.

<p align="center">
    <img src="./Images/9.jpg" alt="1.jpg" width="40%" height="40%">
</p>

### Set Up AWS Cloud9 Environment
In this lab, we use AWS Cloud9 which is a cloud IDE intergrating programming languages and useful tools. A cloud9 environment is based on an EC2 instance. We can  develop applications with a browser anywhere.

1. Sign in to the AWS Management Console, and then open [AWS Cloud9 console](https://console.aws.amazon.com/cloud9/).

2. If prompted, type the email address for the AWS account root user, and then choose Next.

3. If a welcome page is displayed, for **New AWS Cloud9 environment**, choose **Create environment**. Otherwise, choose **Create environment**.

4. On the **Name environment**	page, type  **Yourname-SQS-1** for your environment. Optionally add a description to your environment.

<p align="center">
    <img src="./Images/13.jpg" alt="1.jpg" width="40%" height="40%">
</p>

5. Step 2. Configure settings, leave everything as default but for Network Setting, please select you own vpc that have you prepared before. Then, Select **Next Step**.

<p align="center">
    <img src="./Images/14.jpg" alt="1.jpg" width="40%" height="40%">
</p>

6. Select **Create environment**. It might take 30~60 seconds to create your environment.

7. Because we want to accomplish access control by attaching a role ourself, we need to **turn off** the Cloud9 temporarily provided IAM credentials first.

8. In [Amazon EC2 console](https://console.aws.amazon.com/ec2/v2/home?#Instances:sort=instanceId), right-Select the EC2 instance named with **`aws-cloud9`** prefix and Select **Instance Settings** -> **Attach/Replace IAM Role**.

9. Select **Create new IAM role**.

10. Select **Create role**.

11. Select **EC2** then Select **Next: Permissions**. Because Cloud9 is based on Amazon EC2, therefore we need to choose EC2.

12. Search and select Polices :
      * AdministratorAccess

13. Next: Review.

14. In **Role Name** field, type **AllowEC2Admin** and Select **Create Role**.

15. Back to Attach/Replace IAM Role panel, Select **Refresh** button, **select the role we just create** and Select **Apply**.

16. Go to Security group of Cloud9, edit the inbound by add *all traffic* from *anywhere*

<p align="center">
    <img src="./Images/19.jpg" alt="1.jpg" width="40%" height="40%">
</p>

<p align="center">
    <img src="./Images/20.jpg" alt="1.jpg" width="40%" height="40%">
</p>

17. Back to Cloud9 console, upload and unzip the file [website.zip](./Materials/website.zip) .

18. Please create the second EC2 Cloud9 by re-do it again step 1-17 with name of Cloud 9 is **Yourname-SQS-1**

19. Please take a note of both EC2 Cloud9 IP 

<p align="center">
    <img src="./Images/19.jpg" alt="10.jpg" width="40%" height="40%">
</p>

<p align="center">
    <img src="./Images/20.jpg" alt="11.jpg" width="40%" height="40%">
</p>

20. Please change __<SQS_QUEUE_URL>__ and __<IP ADDRESS ANOTHER EC2>__ in __script.js__ inside website folder. (in **Yourname-SQS**, fill it with **Yourname-SQS2** IP Address and so).

<p align="center">
    <img src="./Images/15.jpg" alt="1.jpg" width="40%" height="40%">
</p>

<p align="center">
    <img src="./Images/16.jpg" alt="1.jpg" width="40%" height="40%">
</p>

21. Type command like in the picture below in both EC2 terminal.

### Test
1. Open in browser http://<IP_ADDRESS>:3000/message of both EC2.
2. Try send message between each other

<p align="center">
    <img src="./Images/21.jpg" alt="1.jpg" width="40%" height="40%">
</p>

<p align="center">
    <img src="./Images/22.jpg" alt="1.jpg" width="40%" height="40%">
</p>

<p align="center">
    <img src="./Images/23.jpg" alt="1.jpg" width="40%" height="40%">
</p>

<p align="center">
    <img src="./Images/25.jpg" alt="1.jpg" width="40%" height="40%">
</p>



### Clean Up

To delete the AWS resources, perform the tasks below in order:

1. Terminate both Cloud9
2. Delete Queue of SQS


## Conclusion
Congratulations! We now have learned how to:
- How to make the Live Chat by SQS
- How to integrated SQS with Cloud9 with javascript
- How to make  build highly scalable EC2 applications
- How to make scalable message queuing service that enables asynchronous message-based communication between distributed components of an application








